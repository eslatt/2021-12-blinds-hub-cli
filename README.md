### Background

I intend to control my blinds with my software via the Rollease Pulse 2 hub using the open pythong libray xyz

### Challenges

aiopulse2 using the pythong aiosync library.. which seems to be a task queue approach to parallel processing kind of like GSS

demo.py uses a HubPromp class that queues up tasks based on responses to a prompt

the challenge right now is replacing the prompt queue with a task queue like 

* connect a hub
* wait for the contents of the hub to be populated
* issue a command to a device
* wait for the device to apply the command

I can add the connect to the queu.. what I really want to do is wait until the job queue is empty after I add something to it

### References

### [aiopulse2](https://pypi.org/project/aiopulse2/)

pip3 install aiopulse2

[github project](https://github.com/sillyfrog/aiopulse2)

### Progress

found the library

installed the library

found demo.py

ran demo.py

ran list command

nothing

you have to connect 192.168.1.127 before you can list

you get the ip address using the app