import asyncio
 # import cmd
 # import functools
 # import logging
 # import json
 # from typing import Any, Callable, Optional
import time
 # 
import aiopulse2
 # from aiopulse2 import _LOGGER

async def hub_update_callback(hub):
  print(f"Hub {hub.name!r} updated")
 # for roller in hub.rollers.values():
 #   roller.callback_subscribe(self.roller_update_callback)

def main():
  print("test_pulse2")

  print('aiopulse2.Hub.')
  hub = aiopulse2.Hub('192.168.1.127')
  print('.aiopulse2.Hub')
  print(hub)

  print('hub.callback_subscribe(hub_update_callback).')
  hub.callback_subscribe(hub_update_callback)
  print('.hub.callback_subscribe(hub_update_callback)')

  print('hub.run.')
  hub.run
  print('.hub.run')

  print('sleep.')
  time.sleep(15)
  print('.sleep')

  print(hub)


main()
